Zadání
======
Úkolem firem je dokončit všech 10 projektů. K tomu, aby mohly na projektech pracovat, si mohou najmout programátory až do výše vlastní kapacity. Každý projekt má svoji náročnost udávanou v normovaných člověkodnech a také cenu, kterou firma obdrží po dokončení projektu. Programátoři pracují na projektech, s tím, že každý má trochu jinou výkonnost. Výkonnost (speed) je udávána v počtu normovaných člověkodnů, které je programátor schopen za den odpracovat. Každý programátor může pracovat maximálně na jednom projektu. Na jednom projektu může pracovat více programátorů.

Vstupní data
------------
### Firmy
- `name` je název firmy,
- `capacity` udává, kolik může firma najmout programátorů,
- `dailyExpenses` jsou denní fixní náklady,
- `budget` je výchozí rozpočet firmy.

### Programátoři
- `name` je jméno programátora,
- `speed` udává, kolik člověkodní programátor odpracuje za den,
- `dailyWage` je denní mzda.

### Projekty
- `name` je název projektu,
- `manDays` je počet člověkodní kolik se musí na projektu odpracovat,
- `price` je částka, kterou firma obdrží při dokončení projektu.

Simulace firem probíhá postupně, každá firma postupně pracuje se stejnými projekty a stejnými programátory (liší se počet programátorů, které mohou najmout). Simulace běhu firmy může skončit v zásadě třemi způsoby.
1. Dodělá všechny projekty a stav se nastaví na _Finished_.
2. Rozpočet se dostane do mínusu a stav se nastaví na _Bankrupt_.
3. Když počet dnů běhu překročí 1000, simulace automaticky skončí a stav firmy se nastaví zpět na _Idle_. V našem testovacím běhu se žádná firma nedostala přes 200 dnů, takže toto slouží spíše jako bezpečnostní pojistka proti nekonečným cyklům.