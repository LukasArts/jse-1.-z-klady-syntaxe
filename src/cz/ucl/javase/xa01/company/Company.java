package cz.ucl.javase.xa01.company;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Company {
    private String name;
    private int capacity;
    private int dailyExpenses;
    private int budget;

    private int days;
    private CompanyState state;
    private List<Programmer> programmers;
    private List<Project> projectsWaiting;
    private List<Project> projectsCurrent;
    private List<Project> projectsDone;

    private Logger logger;

    public Company() {
        this.state = CompanyState.IDLE;
        this.programmers = new ArrayList<Programmer>();
        this.projectsWaiting = new ArrayList<Project>();
        this.projectsCurrent = new ArrayList<Project>();
        this.projectsDone = new ArrayList<Project>();
    }

    public Company(String name, int capacity, int dailyExpenses, int budget) {
        this(); // Volani bezparametrickeho konstruktoru
        this.name = name;
        this.capacity = capacity;
        this.dailyExpenses = dailyExpenses;
        this.budget = budget;
        this.logger = Logger.INSTANCE;
    }

    /**
     * Nacte vsechny projekty do kolekce projectsWaiting.
     *
     * @param projects nacitane projekty
     */
    public void allocateProjects(List<Project> projects) {
        projectsWaiting.addAll(projects);
    }

    /**
     * Najme tolik programatoru, kolik cini kapacita firmy a ulozi je
     * do kolekce programmers.
     *
     * @param programmersAvailable kolekce programatoru k dispozici
     */
    public void allocateProgrammers(List<Programmer> programmersAvailable) {
        // Z parametru programmersAvailable vyberte prvnich capacity programatoru
        // a vlozte je do kolekce (atributu) programmers.
        for (int c = capacity; c > 0 && !programmersAvailable.isEmpty(); --c) {
            Programmer programmer = programmersAvailable.get(0);
            programmers.add(programmer);
        }
    }

    /**
     * Zkontroluje/upravi stav hotovych projektu a upravi rozpocet.
     */
    public void checkProjects() {
        // Z kolekce projectsCurrent odeberte ty projekty, ktere jsou dokoncene.
        // Dokonceny projekt je ten, ktery ma manDaysDone >= manDays.
        // Zaroven temto projektum nastavte spravny stav,
        // pridejte je do projectsDone a do rozpoctu firmy prictete utrzene
        // penize za projekt.
        for (Iterator<Project> projectIterator = projectsCurrent.listIterator(); projectIterator.hasNext(); ) {
            Project project = projectIterator.next();
            if (project.getManDaysDone() >= project.getManDays()) {
                project.setState(ProjectState.DONE);
                projectsDone.add(project);
                budget += project.getPrice();
                projectIterator.remove();
            }
        }
    }

    /**
     * Uvolni programatory z hotovych projektu.
     */
    public void checkProgrammers() {
        // Uvolnete programatory pracujici na projektech, ktere jsou dokonceny.
        for (Programmer programmer : programmers) {
            if (projectsDone.contains(programmer.getProject()))
                programmer.clearProject();
        }
    }

    /**
     * Priradi nove projekty programatorum, kteri nepracuji.
     */
    public void assignNewProjects() {
        // Nastavte projekty programatorum, kteri aktualne nepracuji na zadnem
        // projektu.
        // Pro kazdeho volneho programatora hledejte projekt k prirazeni
        // nasledujicim zpusobem:
        // - Pokud existuje nejaky projekt v projectsWaiting, vyberte prvni
        // takovy. Nezapomente projektu zmenit stav a presunout jej do projectsCurrent.
        // - Pokud ne, vyberte takovy projekt z projectsCurrent, na kterem zbyva
        // nejvice nedodelane prace.
        for (Programmer programmer : programmers) {
            if (programmer.getProject() == null) {
                if (!projectsWaiting.isEmpty()) {
                    Project project = projectsWaiting.get(0);
                    project.setState(ProjectState.CURRENT);
                    projectsCurrent.add(project);
                    programmer.assignProject(project);
                    projectsWaiting.remove(project);
                } else { // najdeme nejnehotovější projectsCurrent
                    double maxManDaysToDo = Double.NEGATIVE_INFINITY;
                    for (Project project : projectsCurrent) {
                        if (project.getManDays() - project.getManDaysDone() > maxManDaysToDo)
                            maxManDaysToDo = project.getManDays() - project.getManDaysDone();
                    }

                    for (Project project : projectsCurrent) {
                        if (project.getManDays() - project.getManDaysDone() == maxManDaysToDo) {
                            programmer.assignProject(project);
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Preda praci programatoru projektum a upravi rozpocet firmy.
     */
    public void programmersWork() {
        // Projdete vsechny programatory a predejte jejich denni vykon (praci)
        // projektum (pouziti metody writeCode).
        // Zaroven snizte aktualni stav financi firmy o jejich denni mzdu a
        // rovnez o denni vydaje firmy.
        for (Programmer programmer : programmers) {
            programmer.writeCode();
            budget -= programmer.getDailyWage();
        }
        budget -= dailyExpenses;
    }

    /**
     * Zkontroluje a nastavi spravny stav firmy.
     */
    public void checkCompanyState() {
        // Pokud je aktualni stav financi firmy zaporny, nastavte stav firmy na
        // Bankrupt.
        // Pokud ne a zaroven pokud jsou jiz vsechny projekty hotove, nastavte
        // stav firmy na Finished.
        if (budget < 0)
            state = CompanyState.BANKRUPT;
        else if (projectsWaiting.isEmpty() && projectsCurrent.isEmpty())
            state = CompanyState.FINISHED;
    }

    /**
     * Spusteni simulace. Simulace je ukoncena pokud je stav firmy Bankrupt nebo
     * Finished, nebo pokud simulace bezi dele nez 1000 dni.
     */
    public void run() {
        logger.log("Company " + name + ", started with budget: " + budget);
        state = CompanyState.RUNNING;
        while (state != CompanyState.BANKRUPT && state != CompanyState.FINISHED && days <= 1000) {
            days++;
            assignNewProjects();
            programmersWork();
            checkProjects();
            checkProgrammers();
            checkCompanyState();
        }

        if (state == CompanyState.RUNNING) {
            state = CompanyState.IDLE;
        }
    }

    public void printResult() {
        System.out.println("Company name: " + name);
        System.out.println("Days running: " + days);
        System.out.println("Final budget: " + budget);
        System.out.println("Final state: " + state);
        System.out.println("Count of projects done: " + projectsDone.size() + "\n");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getDailyExpenses() {
        return dailyExpenses;
    }

    public void setDailyExpenses(int dailyExpenses) {
        this.dailyExpenses = dailyExpenses;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }
}
