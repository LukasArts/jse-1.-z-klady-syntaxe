package cz.ucl.javase.xa01.company;

public class Employee {
    protected String name;
    protected int dailyWage;

    public Employee(String name, int dailyWage) {
        this.dailyWage = dailyWage;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDailyWage() {
        return dailyWage;
    }

    public void setDailyWage(int dailyWage) {
        this.dailyWage = dailyWage;
    }
}
