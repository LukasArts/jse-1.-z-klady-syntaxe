package cz.ucl.javase.xa01.company;

/**
 * Poskytuje funkcionalitu záznamníku.
 * <p>
 * Implementace Singleton anti-pattern pomocí <code>enum</code> elegantně řeší thread-safety i serializaci.
 */
public enum Logger {
    INSTANCE;

    public void log(String message) {
        System.out.println("Logger: " + message);
    }
}
